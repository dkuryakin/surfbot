SurfBot is a lightweight WebKit browser with python control.

Example of usage::

    from surfbot import WebApplication

    app = WebApplication(
        viewport_size=(800, 600),
        log_level=logging.INFO,
        debug=True
    )

    app.load_url_in_tab("http://ya.ru", True)
    app.remember_tabs()

    selector = 'input[name=text]'
    e = app.get_main_web_page().find_elements(selector)
    e[0].move_mouse(offset_x=10)
    e[0].click_mouse(offset_x=10)
    e[0].type(u'qwerty\n', wait_for_page_loaded=True)

    selector = 'ol[class~=b-serp-list] > li[class~=b-serp-item]:not([class*="z-"]) > h2[class~=b-serp-item__title] > a[class~=b-serp-item__title-link]'
    e = app.get_main_web_page().find_elements(selector)
    e[9].move_mouse(offset_x=10)
    e[9].click_mouse(offset_x=10, wait_for_page_loaded=True)

    app.sleep(10000)
    app.close_new_tabs()
    app.exit()
