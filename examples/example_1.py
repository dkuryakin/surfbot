#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from surfbot import WebApplication

app = WebApplication(
    viewport_size=(1500, 800),
    log_level=logging.INFO,
    debug=True
)

app.load_url_in_tab("http://ya.ru", True, open_kwargs={'headers': {'Referer': 'http://mail.ru'}})
app.remember_tabs()

dump = app.dumps_profile()
print dump
app.loads_profile(dump)

e = app.get_main_web_page().switch_to().find_elements("input[name=text]")
e[0].move_mouse(offset_x=10)
e[0].click_mouse(offset_x=10)
e[0].type(u'гангнам')
app.sleep(10000)

ee = app.get_main_web_page().switch_to().find_elements('div > div > ul > li[class~=b-autocomplete-item]')
if ee:
    ee[-1].move_mouse(offset_x=10)
    ee[-1].click_mouse(offset_x=10)
else:
    e[0].type(u' стайл ютуб\n')

app.get_main_web_page().wait_for_selector('div.b-summary__more')

e = app.get_main_web_page().switch_to().find_elements('ol[class~=b-serp-list] > li[class~=b-serp-item]:not([class*="z-"]) > h2[class~=b-serp-item__title] > a[class~=b-serp-item__title-link]')

cookies = app.dumps_cookies()
app.loads_cookies(cookies)

e[9].capture_to_file('test.png')
e[9].move_mouse(offset_x=10)
e[9].click_mouse(offset_x=10)
app.get_current_web_page().wait_for_selector('a')

app.sleep(10000)
app.close_new_tabs()

app.sleep(10000)
app.exit()
