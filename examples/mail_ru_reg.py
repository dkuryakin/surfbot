#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import random
import datetime
from surfbot import WebApplication
from surfbot.common import random_string

def sign_up():
    app = WebApplication(viewport_size=(800, 600), open_links_in_new_tabs=False)

    app.load_url_in_tab("http://m.mail.ru/cgi-bin/signup", True)
    app.remember_tabs()
    page = app.get_current_web_page().quick_enabled()

    username = random_string()
    gender = 'male' if random.randint(0, 1) else 'female'
    gender_key = 'Down' if gender == 'female' else 'Down+Up'
    firstname = u'Василий'
    lastname = u'Пупкин'
    day = str(random.randint(1, 28))
    month = ['Down'] * random.randint(1, 12)
    year = str(random.randint(1965, 1995))
    page.fill('input#Username', username).key('Tab+Tab').type(firstname).key('Tab').\
        type(lastname).key('Tab').key(gender_key).key('Tab').type(day).key('Tab').\
        key(month).key('Tab').type(year).key('Tab+Enter', wait_for_page_loaded=True)

    question = ['Down'] * random.randint(0, 8)
    answer = random_string()
    password = random_string()
    page.fill('a[href*=RegStep]', wait_for_page_loaded=True).\
        fill('input#password', password).key('Tab').key(question).key('Tab').\
        type(answer).key('Enter', wait_for_page_loaded=True)

    captcha_text = page.solve_captcha('img[src*="swa.mail.ru"]')
    page.fill('input.input-captcha', captcha_text).key('Tab+Enter', wait_for_page_loaded=True)

    app.exit()

    result = {
        'username': username,
        'gender': gender,
        'firstname': firstname,
        'lastname': lastname,
        'birthday': datetime.date(year=int(year), month=len(month), day=int(day)),
        'question_num': len(question) + 1,
        'answer': answer,
        'password': password,
        'captcha_text': captcha_text
    }
    return result

account = {
    'username': 'kleBOUCK',
    'question_num': 5,
    'birthday': datetime.date(1978, 4, 14),
    'firstname': u'\u0412\u0430\u0441\u0438\u043b\u0438\u0439',
    'lastname': u'\u041f\u0443\u043f\u043a\u0438\u043d',
    'answer': 'i3wkZu4D',
    'gender': 'male',
    'password': 'HpUHkvmc',
    'captcha_text': '9T69Ok'
}
account = sign_up()
print account

def read_last_mail(account):
    app = WebApplication(viewport_size=(800, 600), open_links_in_new_tabs=False)

    app.load_url_in_tab("http://m.mail.ru/cgi-bin/login", True)
    page = app.get_current_web_page().quick_enabled()

    page.fill('input[name=Login]', account['username']).key('Tab+Tab').\
        type(account['password']).key('Enter', wait_for_page_loaded=True).\
        fill('table#m_msglistcontainer a[href*=readmsg]:nth-child(1)', wait_for_page_loaded=True)

    content = page.find_elements('div.Section1')[0].html
    return content

print read_last_mail(account)