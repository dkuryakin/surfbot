"""
SurfBot
--------

Lightweight WebKit browser with python control.

Features:
  - Supports HTML, Javascript, Flash.
  - Allow total profiles backup/restore ('total' - means with cookies, flash-cookies, cache, etc).
  - All events are organic, not substituted with js-triggers. That means, client has no ability to determine if events were simulated or not.

"""
from setuptools import setup, find_packages

setup(
    name='SurfBot',
    version='0.1.0',
    url='https://bitbucket.org/dkuryakin/surfbot',
    download_url = 'https://bitbucket.org/dkuryakin/surfbot/get/master.tar.gz',
    license='mit',
    author='David Kuryakin',
    author_email='dkuryakin@gmail.com',
    description='Lightweight WebKit browser with python control.',
    keywords='webkit robot bot browser',
    long_description=__doc__,
    data_files=[('surfbot', ['README.rst',])],
    packages=['surfbot'],  # find_packages(exclude=['examples']),
    include_package_data=True,
    zip_safe=False,
    platforms='any',
    requires=[
        #'PySide(>=1.1.2)',
        #'PyQt(>=4.9.6)',
    ],
    install_requires=[
        #'PySide(>=1.1.2)',
        #'PyQt==4.9.6',
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
)