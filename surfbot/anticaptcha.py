# -*- coding: utf-8 -*-

################################################################################

import time
import os
import config
from common import random_string
import httplib
import urllib

################################################################################

class CaptchaFailed(Exception):
    pass

################################################################################

def recognize_captcha_url(captcha_url, key):
    captcha_image = config.TMP_DIR + "/" + random_string(16) + ".jpeg"
    urllib.urlretrieve(captcha_url, captcha_image)
    captcha_text = recognize_captcha(captcha_image, key)
    return captcha_text

################################################################################

def recognize_captcha(captcha_image, key, remove=True):
    status, captcha_text = _recognize_captcha(captcha_image, key)
    if remove: os.remove(captcha_image)
    if status != "OK":
        raise CaptchaFailed
    return captcha_text

################################################################################

def _recognize_captcha(captcha_image, key):
    cap_id = __send_captcha_image(key, captcha_image)
    time.sleep(config.ANTI_CAPTCHA_TIMEOUT)
    res_url = config.ANTI_CAPTCHA_API_URL
    res_url+= "?" + urllib.urlencode({'key': key, 'action': 'get', 'id': cap_id})
    while 1:
        res= urllib.urlopen(res_url).read()
        if res == config.ANTI_CAPTCHA_NOT_READY:
            time.sleep(1)
            continue
        break

    res= res.split('|')
    if len(res) == 2:
        return tuple(res)
    else:
        return ('ERROR', res[0])

################################################################################

def __send_captcha_image(key, captcha_image):
    data = open(captcha_image, 'rb').read()
    boundary = config.ANTI_CAPTCHA_BOUNDARY
    body = '''--%s
Content-Disposition: form-data; name="method"

post
--%s
Content-Disposition: form-data; name="key"

%s
--%s
Content-Disposition: form-data; name="file"; filename="capcha.jpg"
Content-Type: image/pjpeg

%s
--%s--

''' % (boundary, boundary, key, boundary, data, boundary)

    headers = {'Content-type' : 'multipart/form-data; boundary=%s' % boundary}
    h = httplib.HTTPConnection(config.ANTI_CAPTCHA_DOMAIN)
    h.request(config.ANTI_CAPTCHA_SEND_METHOD, config.ANTI_CAPTCHA_SEND_URL, body, headers)
    resp = h.getresponse()
    data = resp.read()
    h.close()
    if resp.status == 200:
        cap_id= int(data.split('|')[1])
        return cap_id
    else:
        return False

        ################################################################################