# -*- coding: utf-8 -*-

import random
import logging

logging.basicConfig()
logger = logging.getLogger('SurfBot')

PYSIDE = False
try:
    import sip
    sip.setapi('QVariant', 2)
    from PyQt4 import QtWebKit
    from PyQt4.QtNetwork import QNetworkRequest, QNetworkAccessManager,\
        QNetworkCookieJar, QNetworkDiskCache, QNetworkCookie, QNetworkProxy,\
        QNetworkReply
    from PyQt4.QtCore import QSize, QByteArray, QUrl, QPoint, Qt, QString,\
        QEvent, QIODevice, QBuffer
    from PyQt4.QtGui import QApplication, QImage, QPainter, QKeyEvent, \
        QCursor, QMouseEvent, QTabWidget
except ImportError:
    try:
        from PySide import QtWebKit
        from PySide.QtNetwork import QNetworkRequest, QNetworkAccessManager,\
            QNetworkCookieJar, QNetworkDiskCache, QNetworkCookie, QNetworkProxy,\
            QNetworkReply
        from PySide.QtCore import QSize, QByteArray, QUrl, QPoint, Qt, QEvent,\
            QIODevice, QBuffer
        from PySide.QtGui import QApplication, QImage, QPainter, QKeyEvent, \
            QCursor, QMouseEvent, QTabWidget
        QString = lambda x: x

        PYSIDE = True
    except ImportError:
        raise Exception("SurfBot requires PySide or PyQt!")

def random_string(length=8):
    chars = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890'
    return ''.join(random.choice(chars) for i in xrange(length))

class Logger(logging.Logger):
    @staticmethod
    def log(message, sender="SurfBot", level="info"):
        if not hasattr(logger, level):
            raise Exception('invalid log level')
        getattr(logger, level)("%s: %s", sender, message)

class Object:
    def __init__(self, **kwargs):
        for k, v in kwargs.iteritems():
            setattr(self, k, v)

    def __unicode__(self):
        params = u", ".join(u'{K}={V}'.format(K=k, V=v) for k, v in self.__dict__.iteritems())
        return u"Object(%s)" % params

    def __str__(self):
        return self.__unicode__().encode('utf-8')

