# -*- coding: utf-8 -*-

import os

ROOT_DIR = os.path.abspath(os.path.dirname(__file__))

ANTI_CAPTCHA_NOT_READY = "CAPCHA_NOT_READY"
ANTI_CAPTCHA_BOUNDARY = "----------33849tv34098um893um398n3yx98nty3784xyr8"
ANTI_CAPTCHA_DOMAIN = "antigate.com"
ANTI_CAPTCHA_API_URL = "http://%s/res.php" % ANTI_CAPTCHA_DOMAIN
ANTI_CAPTCHA_SEND_URL = "/in.php"
ANTI_CAPTCHA_SEND_METHOD = "POST"
ANTI_CAPTCHA_TIMEOUT = 5

DEFAULT_USERAGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2"
DEFAULT_WAIT_TIMEOUT = 120

DISPLAY_X = 1600
DISPLAY_Y = 900

JQUERY = open(os.path.join(ROOT_DIR, 'jquery.js')).read()

JS_EVENTS = '''
    $(document).unbind('mousemove.suggeter');
    $(window).unbind('scroll.suggeter');
    $(document).unbind('mouseleave.suggeter');
    $(document).unbind('mouseover.suggeter');

    $(document).bind('mousemove.suggeter', function(e) {
        window.suggeter_mouse_x = e.pageX;
        window.suggeter_mouse_y = e.pageY;

        $('.suggeter-cursor').remove();
        var x = Math.ceil(window.suggeter_mouse_x || 0) - Math.ceil(window.suggeter_last_scroll_left || 0);
        var y = Math.ceil(window.suggeter_mouse_y || 0) - Math.ceil(window.suggeter_last_scroll_top || 0);
        $('body').append('<div class="suggeter-cursor" style="width:21px; height:3px; background-color:#000; position:fixed; top:' +
                         (y-11) + 'px; left:' + (x-10) + 'px; z-index:1000000;"></div>');
        $('body').append('<div class="suggeter-cursor" style="width:21px; height:3px; background-color:#000; position:fixed; top:' +
                         (y+9) + 'px; left:' + (x-10) + 'px; z-index:1000000;"></div>');
        $('body').append('<div class="suggeter-cursor" style="width:3px; height:21px; background-color:#000; position:fixed; top:' +
                         (y-10) + 'px; left:' + (x-11) + 'px; z-index:1000000;"></div>');
        $('body').append('<div class="suggeter-cursor" style="width:3px; height:21px; background-color:#000; position:fixed; top:' +
                         (y-10) + 'px; left:' + (x+9) + 'px; z-index:1000000;"></div>');

        //console.log(window.suggeter_mouse_x + ', ' + window.suggeter_mouse_y);
    });
    $(window).bind('scroll.suggeter', function(e) {
        window.suggeter_last_scroll_top = window.suggeter_last_scroll_top || 0;
        window.suggeter_last_scroll_left = window.suggeter_last_scroll_left || 0;
        scroll_top = $(document).scrollTop()
        scroll_left = $(document).scrollLeft()
        if (scroll_left != window.suggeter_last_scroll_left) {
            window.suggeter_mouse_x += scroll_left - window.suggeter_last_scroll_left;
            window.suggeter_last_scroll_left = scroll_left;
        }
        if (scroll_top != window.suggeter_last_scroll_top) {
            window.suggeter_mouse_y += scroll_top - window.suggeter_last_scroll_top;
            window.suggeter_last_scroll_top = scroll_top;
        }
        //console.log(window.suggeter_mouse_x + ', ' + window.suggeter_mouse_y);
    });
    $(document).bind('mouseleave.suggeter', function(e) {
        window.suggeter_mouse_x = undefined;
        window.suggeter_mouse_y = undefined;
        //console.log(window.suggeter_mouse_x + ', ' + window.suggeter_mouse_y);
    });
    $(document).bind('mouseover.suggeter', function(e) {
        window.suggeter_mouse_x = e.pageX;
        window.suggeter_mouse_y = e.pageY;
        //console.log(window.suggeter_mouse_x + ', ' + window.suggeter_mouse_y);
    });

    function viewport_geometry() {
        var w = $(window);
        return {x: w.width(), y: w.height()};
    }

    function document_geometry() {
        var w = $(document);
        return {x: w.width(), y: w.height()};
    }
'''
