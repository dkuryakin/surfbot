# -*- coding: utf-8 -*-

import random

class Curvers:
    @staticmethod
    def linear(x1, y1, x2, y2, steps):
        xstep = float(x2 - x1) / steps
        ystep = float(y2 - y1) / steps

        def _curve():
            for step in xrange(steps):
                x = int(x1 + (step + 1) * xstep)
                y = int(y1 + (step + 1) * ystep)
                yield x, y
        return _curve()

    @staticmethod
    def nonlinear(x1, y1, x2, y2, steps):
        for attempt in xrange(30):
            try:
                return Curvers._nonlinear(x1, y1, x2, y2, steps)
            except:
                pass
        return Curvers.linear(x1, y1, x2, y2, steps)

    @staticmethod
    def _nonlinear(x1, y1, x2, y2, steps):
        while True:
            x3 = random.randint(min(x1, x2)-10, max(x1, x2)+10)
            y3 = random.randint(min(y1, y2)-10, max(y1, y2)+10)
            if x3 == x1 or x3 == x2:
                continue
            if y3 == y1 or y3 == y2:
                continue
            break

        def func_gen(x1, y1, x2, y2, x3, y3):
            a = ((y3-y1) - (y2-y1)*(x3-x1)/float(x2-x1)) / ((x3*x3-x1*x1) - (x2*x2-x1*x1)*(x3-x1)/float(x2-x1))
            b = ((y2-y1) - a*(x2*x2-x1*x1)) / float(x2-x1)
            c = y1 - a*x1*x1 - b*x1
            def func(x):
                return a*x*x + b*x + c
            return func

        if abs(x1-x2) > abs(y1-y2):
            xstep = float(x2-x1) / steps
            func = func_gen(x1, y1, x2, y2, x3, y3)
            def _curve():
                for step in xrange(steps):
                    x = int(x1 + (step + 1) * xstep)
                    y = int(func(x))
                    if x<0: x=0
                    if y<0: y=0
                    yield x, y
            return _curve()
        else:
            ystep = float(y2-y1) / steps
            func = func_gen(y1, x1, y2, x2, y3, x3)
            def _curve():
                for step in xrange(steps):
                    y = int(y1 + (step + 1) * ystep)
                    x = int(func(y))
                    if x<0: x=0
                    if y<0: y=0
                    yield x, y
            return _curve()
