# -*- coding: utf-8 -*-
from cookielib import CookieJar, Cookie
import re
import shutil
import base64
import curvers
import config
import os
import time
import random
import codecs
import logging
import tempfile
import json
from functools import wraps
from web_page import WebPage, HttpResource
from common import logger, QApplication, QtWebKit, QString, QNetworkProxy, \
    QNetworkCookieJar, QNetworkDiskCache, Qt, QNetworkAccessManager, PYSIDE, \
    random_string, QNetworkCookie, QByteArray, Logger, QNetworkRequest, QTabWidget,\
    QBuffer, QIODevice, QNetworkReply

class QCustomNetworkAccessManager(QNetworkAccessManager):
    def __init__(self, *args, **kwargs):
        self.post_data_handlers = {}
        self.blocked_urls = {}
        super(QCustomNetworkAccessManager, self).__init__(*args, **kwargs)

    def bind_post_data_handler(self, url_mask, handler):
        self.post_data_handlers[url_mask] = handler

    def unbind_post_data_handler(self, url_mask):
        del self.post_data_handlers[url_mask]

    def block_url(self, url_mask):
        self.blocked_urls[url_mask] = None

    def unblock_url(self, url_mask):
        del self.blocked_urls[url_mask]

    def createRequest(self, *args, **kwargs):
        op_code, request, io_device = args
        url = request.url().toString()

        for url_mask in self.blocked_urls.iterkeys():
            if re.search(url_mask, url):
                Logger.log("    BLOCK URL: " + url)
                return super(QCustomNetworkAccessManager, self).get(QNetworkRequest())

        if io_device is not None:
            post_data = str(io_device.peek(1024 * 1024))

            for url_mask, handler in self.post_data_handlers.iteritems():
                if re.search(url_mask, url):
                    Logger.log("    HANDLE POST DATA FOR URL: " + url)
                    post_data = handler(post_data)

            io_device = QBuffer(io_device)
            io_device.setData(QByteArray(post_data))
            args = (op_code, request, io_device)

        return super(QCustomNetworkAccessManager, self).createRequest(*args, **kwargs)

class WebApplication():
    def __init__(self,
                 user_agent=config.DEFAULT_USERAGENT,
                 wait_timeout=config.DEFAULT_WAIT_TIMEOUT,
                 wait_callback=None,
                 log_level=logging.WARNING,
                 viewport_size=(config.DISPLAY_X, config.DISPLAY_Y),
                 ignore_ssl_errors=True,
                 cache_dir=os.path.join(tempfile.gettempdir(), 'webkit-network-cache-' + random_string()),
                 plugins_enabled=True,
                 java_enabled=True,
                 plugin_path=['/usr/lib/mozilla/plugins'],
                 homedir=os.path.join(tempfile.gettempdir(), 'webkit-home-dir-' + random_string()),
                 profile_dump=None,
                 proxy=None,
                 debug=False,
                 focus_new_tabs=True,
                 open_links_in_new_tabs=True,
                 antigate_key=None,
                 wait_for_page_load_when_open=False,
                 q_network_cookie_jar=None):
        logger.setLevel(log_level)
        self.wait_for_page_load_when_open = wait_for_page_load_when_open
        self.antigate_key = antigate_key
        self.focus_new_tabs = focus_new_tabs
        self.open_links_in_new_tabs = open_links_in_new_tabs

        self.http_resources = {}
        self.tabs_map = {}
        self.web_pages = {}
        self.pages_id = 0

        self.orig_homedir = os.environ.get('HOME', None)
        os.environ['HOME'] = homedir
        self.homedir = homedir

        self.debug = debug
        self.user_agent = user_agent
        self.wait_timeout = wait_timeout
        self.wait_callback = wait_callback
        self.ignore_ssl_errors = ignore_ssl_errors

        self.application = QApplication.instance() or QApplication(['SurfBot'])
        if plugin_path:
            for p in plugin_path:
                self.application.addLibraryPath(p)

        #QtWebKit.QWebSettings.setMaximumPagesInCache(0)
        #QtWebKit.QWebSettings.setObjectCacheCapacities(0, 0, 0)
        QtWebKit.QWebSettings.globalSettings().setAttribute(QtWebKit.QWebSettings.LocalContentCanAccessRemoteUrls, True)
        QtWebKit.QWebSettings.enablePersistentStorage(QString(os.environ['HOME'] + '/.webkit-local-storage'))

        self.manager = QCustomNetworkAccessManager()
        if proxy is not None:
            ip, port = proxy.strip().split(":")
            port = int(port)
            qproxy = QNetworkProxy()
            qproxy.setHostName(QString(ip))
            qproxy.setPort(port)
            qproxy.setType(QNetworkProxy.HttpProxy)

            #self.manager.setProxy(qproxy)
            QNetworkProxy.setApplicationProxy(qproxy)

        self.manager.finished.connect(self._request_ended)
        self.manager.sslErrors.connect(self._on_manager_ssl_errors)

        # Cache
        self.cache_dir = cache_dir
        self.cache = QNetworkDiskCache()
        self.cache.setCacheDirectory(cache_dir)
        self.manager.setCache(self.cache)

        # Cookie jar
        if isinstance(q_network_cookie_jar, QNetworkCookieJar):
            self.cookie_jar = q_network_cookie_jar
        else:
            self.cookie_jar = QNetworkCookieJar()

        self.manager.setCookieJar(self.cookie_jar)

        # User Agent
        self.user_agent = self.user_agent

        self.manager.authenticationRequired.connect(self._authenticate)
        self.manager.proxyAuthenticationRequired.connect(self._authenticate)

        if plugins_enabled:
            QtWebKit.QWebSettings.globalSettings().setAttribute(QtWebKit.QWebSettings.PluginsEnabled, True)
        if java_enabled:
            QtWebKit.QWebSettings.globalSettings().setAttribute(QtWebKit.QWebSettings.JavaEnabled, True)

        QtWebKit.QWebSettings.globalSettings().setAttribute(QtWebKit.QWebSettings.JavascriptEnabled, True)
        QtWebKit.QWebSettings.globalSettings().setAttribute(QtWebKit.QWebSettings.JavascriptCanOpenWindows, True)
        QtWebKit.QWebSettings.globalSettings().setAttribute(QtWebKit.QWebSettings.JavascriptCanCloseWindows, True)
        QtWebKit.QWebSettings.globalSettings().setAttribute(QtWebKit.QWebSettings.JavascriptCanAccessClipboard, True)

        self.loads_profile(profile_dump)
        self.tabs = QTabWidget()
        self.tabs.setFixedSize(*viewport_size)
        self.tabs.show()

        self.old_tabs = set()

    def block_url(self, url_mask):
        self.manager.block_url(url_mask)

    def unblock_url(self, url_mask):
        self.manager.unblock_url(url_mask)

    def bind_post_data_handler(self, url_mask, handler):
        self.manager.bind_post_data_handler(url_mask, handler)

    def unbind_post_data_handler(self, url_mask):
        self.manager.unbind_post_data_handler(url_mask)

    def get_current_tab(self):
        idx = self.tabs.currentIndex()
        return dict((v, k) for k, v in self.tabs_map.iteritems())[idx]

    def get_current_web_page(self):
        return self.get_tab_web_page(self.get_current_tab())

    def get_main_web_page(self):
        return self.get_tab_web_page(self.main_tab)

    def get_tab_web_page(self, tab_id):
        return self.web_pages[tab_id]

    def get_tabs(self):
        return self.web_pages.keys()

    def get_main_tab(self):
        return self.main_tab

    def switch_to_tab(self, tab_id):
        self.tabs.setCurrentIndex(self.tabs_map[tab_id])

    def close_tab(self, tab_id):
        self.web_pages[tab_id].exit()

    def remember_tabs(self):
        self.old_tabs = set(self.get_tabs())

    def get_new_tabs(self):
        tabs = set(self.get_tabs())
        return list(tabs - self.old_tabs)

    def switch_to_main_tab(self):
        self.switch_to_tab(self.main_tab)

    def close_tabs(self, exclude_tabs=[]):
        for tab in self.get_tabs():
            if tab in exclude_tabs:
                continue
            self.close_tab(tab)

    def close_not_main_tabs(self):
        self.close_tabs([self.main_tab])

    def close_new_tabs(self):
        self.close_tabs(self.get_new_tabs())

    def load_url_in_tab(self, url, set_as_main_tab=False,
                        focus_new_tab=None, open_links_in_new_tabs=None,
                        focus_links_tabs=None, open_kwargs={}):
        if focus_links_tabs is None: focus_links_tabs = self.focus_new_tabs
        if open_links_in_new_tabs is None: open_links_in_new_tabs = self.open_links_in_new_tabs
        web_page = WebPage(self, open_links_in_new_tabs, focus_links_tabs)

        self.web_pages[web_page.page_id] = web_page
        view = web_page.webview
        tab_id = self.tabs.addTab(view, 'loading...')
        self.tabs_map[web_page.page_id] = tab_id
        if focus_new_tab is None: focus_new_tab = self.focus_new_tabs

        if focus_new_tab:
            self.tabs.setCurrentIndex(tab_id)
        if set_as_main_tab:
            self.main_tab = web_page.page_id
        web_page.open(url, **open_kwargs)
        return web_page

    def __del__(self):
        self.exit()

    def _request_ended(self, reply):
        for web_page in self.web_pages.itervalues():
            if reply.request().originatingObject() != web_page.main_frame:
                continue
            if reply.attribute(QNetworkRequest.HttpStatusCodeAttribute):
                self.http_resources[web_page.page_id] = self.http_resources.get(web_page.page_id, [])
                self.http_resources[web_page.page_id].append(HttpResource(reply, self.cache))

    def _on_manager_ssl_errors(self, reply, errors):
        url = unicode(reply.url().toString())
        if self.ignore_ssl_errors:
            reply.ignoreSslErrors()
        else:
            Logger.log('SSL certificate error: %s' % url, level='warning')

    def get_page_id(self):
        self.pages_id += 1
        return self.pages_id

    @property
    def cookies(self):
        return self.cookie_jar.allCookies()

    def delete_cookies(self):
        self.cookie_jar.setAllCookies([])

    def exit(self):
        if self.orig_homedir is not None:
            os.environ['HOME'] = self.orig_homedir
        os.system("rm -rf " + self.cache_dir)
        os.system("rm -rf " + self.homedir)

        if PYSIDE:
            pid = os.getpid()
            os.kill(pid, 9)
        self.application.quit()

    def _authenticate(self, mix, authenticator):
        if self._auth_attempt == 0:
            username, password = self._auth
            authenticator.setUser(username)
            authenticator.setPassword(password)
            self._auth_attempt += 1

    def loop(self):
        self.application.exec_()

    def loop_callback(self, delay, callback=None, step=10):
        if callback is None:
            callback = lambda:None

        start = time.time()
        while time.time() - start < delay / 1000.0:
            callback()
            self.application.processEvents()
            time.sleep(step / 1000.0)

    def sleep(self, delay, step=10):
        self.loop_callback(delay, step=step)

    def dumps_cookies(self):
        cookies = [str(cookie.toRawForm()) for cookie in self.cookies]
        return json.dumps(cookies)

    def loads_cookies(self, cookies):
        cookies = json.loads(cookies)
        cookies = [QNetworkCookie(QByteArray(cookie)) for cookie in cookies]
        self.cookie_jar.setAllCookies(cookies)

    def dumps_profile(self):
        arc = self.homedir + "/dumps_profile_" + random_string() + ".tar.gz"
        files = " ".join(os.listdir(self.homedir))
        os.system('tar -C "{HOME}" -zcvf "{ARC}" {FILES}'.format(
            HOME=self.homedir,
            ARC=arc,
            FILES=files
        ))
        dump = base64.encodestring(open(arc, "rb").read())
        os.remove(arc)
        dump = {
            'cookies': self.dumps_cookies(),
            'profile': dump
        }
        return json.dumps(dump)

    def loads_profile(self, dump=None):
        if dump is None:
            return
        dump = json.loads(dump)
        cookies, dump = dump['cookies'], base64.decodestring(dump['profile'])
        self.loads_cookies(cookies)
        arc = self.homedir + "/dumps_profile_" + random_string() + ".tar.gz"
        dump_file = open(arc, "wb")
        dump_file.write(dump)
        dump_file.close()
        os.system('tar -C "{HOME}" -zxvf "{ARC}"'.format(
            HOME=self.homedir,
            ARC=arc
        ))
        os.remove(arc)
