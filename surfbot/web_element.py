# -*- coding: utf-8 -*-
import shutil
import base64
from common import Object, QCursor, QPoint, Qt, QEvent, QKeyEvent, QMouseEvent, random_string
import curvers
import config
import os
import time
import random
import codecs
import logging
import tempfile
import json
from functools import wraps

class WebElement:
    def __init__(self, web_page, selector, offset, element):
        self.web_page = web_page
        self.view = web_page.webview
        self.page = web_page.page
        self.frame = web_page.main_frame
        self.selector = selector
        self.offset = offset
        self.element = element

    @classmethod
    def generate(cls, web_page, selector):
        elements = web_page.main_frame.findAllElements(selector)
        _elements = []
        for i, e in enumerate(elements):
            _elements.append(cls(web_page, selector, i, e))
        return _elements

    def _process_events(self):
        self.web_page.application.processEvents()

    @property
    def geometry(self):
        g = self.element.geometry()
        return Object(x=g.width(), y=g.height())

    @property
    def html(self):
        return self.element.toPlainText()

    @property
    def location(self):
        g = self.element.geometry()
        return Object(x=g.x(), y=g.y())

    def move_mouse(self, offset_x=None, offset_y=None, delay=1000,
                   wait_for_page_loaded=False, curve_generator=curvers.Curvers.nonlinear,
                   time_step=10, scroll=True):
        if wait_for_page_loaded:
            self.web_page.loaded = False

        if offset_x is None: offset_x = self.geometry.x / 2
        if offset_y is None: offset_y = self.geometry.y / 2

        if scroll:
            self.scroll_to(offset_x, offset_y)
        ss = self.web_page.scroll_state

        l = self.location
        l = Object(x=l.x-ss.x, y=l.y-ss.y)
        wl = self.web_page.location
        point_b = QPoint(wl.x + l.x + offset_x, wl.y + l.y + offset_y)
        point_a = QCursor.pos()
        dist = ((point_a.x() - point_b.x())**2 + (point_a.y() - point_b.y())**2)**0.5
        steps = int(delay * dist * 0.001 / time_step) + 1
        curve = curve_generator(point_a.x(), point_a.y(), point_b.x(), point_b.y(), steps)

        for x, y in curve:
            QCursor.setPos(QPoint(x, y))
            self.web_page.sleep(time_step)

        if wait_for_page_loaded:
            self.web_page.wait_for_page_loaded()

    def click_mouse(self, offset_x=None, offset_y=None, button='left', double_click=False,
                    delay=100, wait_for_page_loaded=False, scroll=True):
        if wait_for_page_loaded:
            self.web_page.loaded = False

        if offset_x is None: offset_x = self.geometry.x / 2
        if offset_y is None: offset_y = self.geometry.y / 2

        if scroll:
            self.scroll_to(offset_x, offset_y)
        ss = self.web_page.scroll_state

        l = self.location
        l = Object(x=l.x-ss.x, y=l.y-ss.y)
        point = QPoint(l.x + offset_x, l.y + offset_y)

        if button == 'left':
            btn = Qt.LeftButton
        elif button == 'right':
            btn = Qt.RightButton

        buttons = Qt.LeftButton | Qt.RightButton
        if double_click:
            event = QMouseEvent(QEvent.MouseButtonDblClick, point, btn, buttons, self.web_page.application.keyboardModifiers())
            self.web_page.application.sendEvent(self.view, event)
        else:
            event = QMouseEvent(QEvent.MouseButtonPress, point, btn, buttons, self.web_page.application.keyboardModifiers())
            self.web_page.application.sendEvent(self.view, event)
            event = QMouseEvent(QEvent.MouseButtonRelease, point, btn, buttons, self.web_page.application.keyboardModifiers())
            self.web_page.application.sendEvent(self.view, event)
        self.web_page.sleep(delay)

        if wait_for_page_loaded:
            self.web_page.wait_for_page_loaded()

    def type(self, *args, **kwargs):
        self.web_page.type(*args, **kwargs)

    def key(self, *args, **kwargs):
        self.web_page.key(*args, **kwargs)

    class FailedToScrollToWebElement(Exception):
        pass

    def scroll_to(self, offset_x=None, offset_y=None, delay=100, wait_for_page_loaded=False):
        if wait_for_page_loaded:
            self.web_page.loaded = False

        if offset_x is None: offset_x = self.geometry.x / 2
        if offset_y is None: offset_y = self.geometry.y / 2

        el = self.location
        el = Object(x=el.x+offset_x, y=el.y+offset_y)
        self._ss = self.web_page.scroll_state
        wg = self.web_page.window_geometry

        ax = min(200, wg.x)
        ay = min(200, wg.y)

        def _scroll(condition, weak_condition, callback, self=self):
            ox, oy = self._ss.x, self._ss.y
            while condition():
                callback()
                self._ss = self.web_page.scroll_state
                if ox == self._ss.x and oy == self._ss.y and condition():
                    if not weak_condition():
                        break
                    raise WebElement.FailedToScrollToWebElement
                ox, oy = self._ss.x, self._ss.y

        _scroll(lambda:(el.x < self._ss.x + ax), lambda:(el.x < self._ss.x), self.web_page.scroll_left)
        _scroll(lambda:(el.x > self._ss.x + wg.x - ax), lambda:(el.x > self._ss.x + wg.x), self.web_page.scroll_right)
        _scroll(lambda:(el.y < self._ss.y + ay), lambda:(el.y < self._ss.y), self.web_page.scroll_up)
        _scroll(lambda:(el.y > self._ss.y + wg.y - ay), lambda:(el.y > self._ss.y + wg.y), self.web_page.scroll_down)
        delattr(self, '_ss')
        self.web_page.sleep(delay)

        if wait_for_page_loaded:
            self.web_page.wait_for_page_loaded()

    def capture(self, scroll=True):
        if scroll:
            self.scroll_to()
        l = self.location
        g = self.geometry
        s = self.web_page.scroll_state
        r = Object(x=l.x-s.x, y=l.y-s.y)
        region = (r.x, r.y, r.x+g.x, r.y+g.y)
        return self.web_page.capture(region=region)

    def capture_to_file(self, path=None, scroll=True):
        if path is None:
            path = os.path.join(tempfile.gettempdir(), 'capture-web-element-' + random_string() + '.png')
        self.capture(scroll=scroll).save(path)
        return path