# -*- coding: utf-8 -*-
import shutil
from anticaptcha import recognize_captcha
from web_element import WebElement
import base64
import curvers
import config
import os
import time
import random
import codecs
import logging
import tempfile
import json
from functools import wraps
from common import QtWebKit, Logger, Object, PYSIDE, QNetworkRequest, \
    QImage, QByteArray, QNetworkAccessManager, QKeyEvent, QEvent, \
    QNetworkCookie, QPainter, QSize, QPoint, QUrl, Qt, random_string, \
    QCursor, QMouseEvent

class _QWebPage(QtWebKit.QWebPage):
    def __init__(self, web_page, *args, **kwargs):
        super(_QWebPage, self).__init__()
        self.web_page = web_page
        self.setNetworkAccessManager(web_page.web_app.manager)
        self.setLinkDelegationPolicy(QtWebKit.QWebPage.DelegateAllLinks)
        self.setForwardUnsupportedContent(True)
        self.setUserAgent(web_page.user_agent)

        self.linkClicked.connect(web_page._link_clicked)
        self.loadFinished.connect(web_page._page_loaded)
        self.loadStarted.connect(web_page._page_load_started)
        self.unsupportedContent.connect(web_page._unsupported_content)

        #self.mainFrame().setScrollBarPolicy(Qt.Vertical, Qt.ScrollBarAlwaysOff)
        #self.mainFrame().setScrollBarPolicy(Qt.Horizontal, Qt.ScrollBarAlwaysOff)

    def chooseFile(self, frame, suggested_file=None):
        return self.web_page._upload_file

    def javaScriptConsoleMessage(self, message, line, source):
        super(_QWebPage, self).javaScriptConsoleMessage(message, line,
            source)
        log_type = "error" if "Error" in message else "info"
        Logger.log("%s(%d): %s" % (source or '<unknown>', line, message),
            sender="Frame", level=log_type)

    def javaScriptAlert(self, frame, message):
        self.web_page._alert = message
        Logger.log("alert('%s')" % message, sender="Frame")

    def javaScriptConfirm(self, frame, message):
        if self.web_page._confirm_expected is None:
            raise Exception('You must specified a value to confirm "%s"' % message)
        confirmation, callback = self.web_page._confirm_expected
        self.web_page._confirm_expected = None
        Logger.log("confirm('%s')" % message, sender="Frame")
        if callback is not None:
            return callback()
        return confirmation

    def javaScriptPrompt(self, frame, message, defaultValue, result=None):
        if self.web_page._prompt_expected is None:
            raise Exception('You must specified a value for prompt "%s"' % message)
        result_value, callback = self.web_page._prompt_expected
        Logger.log("prompt('%s')" % message, sender="Frame")
        if callback is not None:
            result_value = callback()
        if result_value == '':
            Logger.log("'%s' prompt filled with empty string" % message, level='warning')
        self.web_page._prompt_expected = None
        if result is None:
            # PySide
            return True, result_value
        result.append(result_value)
        return True

    def setUserAgent(self, user_agent):
        self.user_agent = user_agent

    def userAgentForUrl(self, url):
        return self.user_agent


def can_load_page(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        expect_loading = False
        if 'expect_loading' in kwargs:
            expect_loading = kwargs['expect_loading']
            del kwargs['expect_loading']
        if expect_loading:
            self.loaded = False
            func(self, *args, **kwargs)
            return self.wait_for_page_loaded()
        return func(self, *args, **kwargs)
    return wrapper


class HttpResource(object):
    def __init__(self, reply, cache, content=None):
        if PYSIDE:
            self.url = reply.url().toString()
        else:
            self.url = reply.url()
        self.content = content
        if self.content is None:
            # Tries to get back content from cache
            buffer = cache.data(self.url)
            if buffer is not None:
                content = buffer.readAll()
                try:
                    self.content = unicode(content)
                except UnicodeDecodeError:
                    self.content = content
        self.http_status = reply.attribute(
            QNetworkRequest.HttpStatusCodeAttribute)
        Logger.log("Resource loaded: %s %s" % (self.url, self.http_status))
        self.headers = {}
        for header in reply.rawHeaderList():
            Logger.log("RESPONSE HEADER: %s: %s" % (header, reply.rawHeader(header)), sender="SurfBot")
            self.headers[unicode(header)] = unicode(reply.rawHeader(header))
        self._reply = reply


class WebPage(object):
    def __init__(self, web_app, open_links_in_new_tabs=True, focus_links_tabs=True):
        self.open_links_in_new_tabs = open_links_in_new_tabs
        self.focus_links_tabs = focus_links_tabs
        self.page_id = web_app.get_page_id()
        self.web_app = web_app
        self.application = web_app.application
        self.user_agent = web_app.user_agent

        self._upload_file = None
        self._alert = None
        self._confirm_expected = None
        self._prompt_expected = None

        self.wait_timeout = web_app.wait_timeout
        self.wait_callback = web_app.wait_callback
        self.loaded = True
        self.quick = False
        self.events = {
            'start_load': [],
            'finish_load': [],
            'click': []
        }

        self.page = _QWebPage(self)
        self.main_frame = self.page.mainFrame()

        if web_app.debug:
            def bind_handlers():
                self.evaluate(config.JQUERY)
                self.evaluate(config.JS_EVENTS)
            self.bind('finish_load', bind_handlers)

        self.webview = QtWebKit.QWebView()
        self.webview.setPage(self.page)
        self.webview.show()

    def __del__(self):
        self.exit()

    def capture(self, region=None, selector=None,
                format=QImage.Format_ARGB32_Premultiplied):
        if region is None and selector is not None:
            region = self.region_for_selector(selector)
        if region:
            x1, y1, x2, y2 = region
            w, h = (x2 - x1), (y2 - y1)
            image = QImage(QSize(x2, y2), format)
            painter = QPainter(image)
            self.main_frame.render(painter)
            painter.end()
            image = image.copy(x1, y1, w, h)
        else:
            image = QImage(self.page.viewportSize(), format)
            painter = QPainter(image)
            self.main_frame.render(painter)
            painter.end()
        return image

    def capture_to(self, path, region=None, selector=None,
                   format=QImage.Format_ARGB32_Premultiplied):
        self.capture(region=region, format=format,
            selector=selector).save(path)

    class confirm:
        def __init__(self, confirm=True, callback=None):
            self.confirm = confirm
            self.callback = callback

        def __enter__(self):
            self._confirm_expected = (self.confirm, self.callback)

        def __exit__(self, type, value, traceback):
            self._confirm_expected = None

    @property
    def content(self):
        return unicode(self.main_frame.toHtml())

    @can_load_page
    def evaluate(self, script):
        return (self.main_frame.evaluateJavaScript("%s" % script),
                self._release_last_resources())

    def evaluate_js_file(self, path, encoding='utf-8'):
        self.evaluate(codecs.open(path, encoding=encoding).read())

    def exists(self, selector):
        return not self.main_frame.findFirstElement(selector).isNull()

    def switch_to(self):
        self.web_app.switch_to_tab(self.page_id)
        return self

    def exit(self):
        self.webview.close()
        index = self.web_app.tabs_map[self.page_id]
        del self.web_app.tabs_map[self.page_id]
        del self.web_app.web_pages[self.page_id]
        del self.web_app.http_resources[self.page_id]
        del self.page
        del self.main_frame
        self.web_app.tabs.removeTab(index)

    def open(self, address, method='get', headers={}, auth=None, body=None):
        body = body or QByteArray()
        try:
            method = getattr(QNetworkAccessManager,
                "%sOperation" % method.capitalize())
        except AttributeError:
            raise Exception("Invalid http method %s" % method)
        request = QNetworkRequest(QUrl(address))
        request.CacheLoadControl(0)

        referer = str(self.main_frame.url().toEncoded()).split("#", 1)[0] or "-"
        if 'Referer' not in headers: headers['Referer'] = referer

        if headers.get('Referer', None) == '-':
            del headers['Referer']

        for header in headers:
            Logger.log("REQUEST EXTRA HEADER: %s: %s" % (header, headers[header]), sender="SurfBot")
            request.setRawHeader(header, headers[header])
        self._auth = auth
        self._auth_attempt = 0  # Avoids reccursion
        self.loaded = False
        self.main_frame.load(request, method, body)
        return self.wait_for_page_loaded() if self.web_app.wait_for_page_load_when_open else self

    class prompt:
        def __init__(self, value='', callback=None):
            self.value = value
            self.callback = callback

        def __enter__(self):
            self._prompt_expected = (self.value, self.callback)

        def __exit__(self, type, value, traceback):
            self._prompt_expected = None

    def region_for_selector(self, selector):
        geo = self.main_frame.findFirstElement(selector).geometry()
        try:
            region = (geo.left(), geo.top(), geo.right(), geo.bottom())
        except:
            raise Exception("can't get region for selector '%s'" % selector)
        return region

    def wait_for(self, condition, timeout_message):
        started_at = time.time()
        while not condition():
            if time.time() > (started_at + self.wait_timeout):
                raise Exception(timeout_message)
            time.sleep(0.01)
            self.application.processEvents()
            if self.wait_callback is not None:
                self.wait_callback()
        return self

    def wait_for_alert(self):
        self.wait_for(lambda: self._alert is not None, 'User has not been alerted.')
        msg = self._alert
        self._alert = None
        #return msg, self._release_last_resources()
        return self

    def wait_for_page_loaded(self):
        self.wait_for(lambda: self.loaded, 'Unable to load requested page')
        resources = self._release_last_resources()
        page = None
        url = self.main_frame.url().toString()
        for resource in resources:
            if url == resource.url:
                page = resource
        #return page, resources
        return self

    def wait_for_selector(self, selector):
        self.wait_for(lambda: self.exists(selector), 'Can\'t find element matching "%s"' % selector)
        #return True, self._release_last_resources()
        return self

    def wait_for_text(self, text):
        self.wait_for(lambda: text in self.content,
            'Can\'t find "%s" in current frame' % text)
        #return True, self._release_last_resources()
        return self

    def _page_loaded(self):
        self.loaded = True
        index = self.web_app.tabs_map[self.page_id]
        self.web_app.tabs.setTabText(index, self.webview.url().host())
        self.web_app.cache.clear()
        for callback in self.events['finish_load']:
            callback()

    def _page_load_started(self):
        index = self.web_app.tabs_map[self.page_id]
        self.web_app.tabs.setTabText(index, 'loading...')
        self.loaded = False
        for callback in self.events['start_load']:
            callback()

    def _link_clicked(self, url):
        for callback in self.events['click']:
            callback()
        ppurl = unicode(self.main_frame.url().toString()).split("#", 1)[0]
        referer = str(self.main_frame.url().toEncoded()).split("#", 1)[0] or "-"
        purl = unicode(url.toString()).split("#", 1)[0]
        Logger.log("REQUEST: '%s'   =>   '%s'" % (ppurl, purl), sender="SurfBot")
        fragment = url.fragment()
        if purl.startswith('javascript:'):
            js = purl.split(':', 1)[1]
            self.evaluate(js)
        else:
            if ppurl != purl:
                if self.open_links_in_new_tabs:
                    self.web_app.load_url_in_tab(url, focus_new_tab=self.focus_links_tabs, open_kwargs={'headers': {'Referer': referer}})
                else:
                    self.open(url, headers={'Referer': referer})
            if fragment and self.exists(fragment):
                elements = WebElement.generate(self, purl)
                elements[0].scroll_to()

    def _release_last_resources(self):
        last_resources = self.web_app.http_resources.get(self.page_id, [])
        self.web_app.http_resources[self.page_id] = []
        return last_resources

    def _unsupported_content(self, reply):
        if reply.attribute(QNetworkRequest.HttpStatusCodeAttribute):
            self.web_app.http_resources[self.page_id] = self.web_app.http_resources.get(self.page_id, [])
            self.web_app.http_resources[self.page_id].append(HttpResource(reply, self.web_app.cache, reply.readAll()))

    def bind(self, event, callback):
        self.events[event] = self.events.get(event, []) + [callback]

    def find_elements(self, selector):
        return WebElement.generate(self, selector)

    def loop_callback(self, delay, callback=None, step=10):
        if callback is None:
            callback = lambda:None

        start = time.time()
        while time.time() - start < delay / 1000.0:
            callback()
            self.application.processEvents()
            time.sleep(step / 1000.0)
        return self

    def sleep(self, delay, step=10):
        self.loop_callback(delay, step=step)
        return self

    @property
    def location(self):
        loc = self.webview.mapToGlobal(QPoint(0, 0))
        return Object(x=loc.x(), y=loc.y())

    @property
    def window_geometry(self):
        g = self.main_frame.geometry()
        return Object(x=g.width(), y=g.height())

    @property
    def document_geometry(self):
        cs = self.main_frame.contentsSize()
        return Object(x=cs.width(), y=cs.height())

    @property
    def scroll_state(self):
        s = self.main_frame.scrollPosition()
        return Object(left=s.x(), top=s.y(), x=s.x(), y=s.y())

    def quick_enabled(self):
        self.quick = True
        return self

    def quick_disabled(self):
        self.quick = False
        return self

    def fill(self, selector, text=None, is_key=False, wait_for_page_loaded=False, offset_x=10, offset_y=None, wait_for_selector=True):
        if wait_for_selector: self.wait_for_selector(selector)
        element = self.find_elements(selector)[0]

        if self.quick:
            extra_opts = {'delay': 0}
        else:
            extra_opts = {}
        element.move_mouse(offset_x=offset_x, offset_y=offset_y, **extra_opts)
        element.click_mouse(offset_x=offset_x, offset_y=offset_y, **extra_opts)
        if text is not None:
            if is_key:
                element.key(text, wait_for_page_loaded=wait_for_page_loaded, **extra_opts)
            else:
                element.type(text, wait_for_page_loaded=wait_for_page_loaded, **extra_opts)
        return self

    def type(self, text, delay=300, wait_for_page_loaded=False):
        if self.quick: delay = 0
        for key in text:
            self.application.sendEvent(self.webview, QKeyEvent(QEvent.KeyPress, 0, self.application.keyboardModifiers(), text=key))
            if wait_for_page_loaded:
                self.loaded = False
            self.sleep(delay * random.random())

        if wait_for_page_loaded:
            self.wait_for_page_loaded()
        return self

    def key(self, key, delay=100, wait_for_page_loaded=False):
        if self.quick: delay = 0
        if wait_for_page_loaded:
            self.loaded = False

        keys = {
            'Enter': Qt.Key_Enter,
            'PageUp': Qt.Key_PageUp,
            'PageDown': Qt.Key_PageDown,
            'Left': Qt.Key_Left,
            'Right': Qt.Key_Right,
            'Up': Qt.Key_Up,
            'Down': Qt.Key_Down,
            'Tab': Qt.Key_Tab,
            'Space': Qt.Key_Space,
            'Ctrl': Qt.Key_Control,
            'L': Qt.Key_L,
            'A': Qt.Key_A
        }
        if isinstance(key, basestring):
            key = [keys[k.strip()] for k in key.split('+')]
        elif isinstance(key, (list, tuple)):
            key = [keys[k.strip()] for k in key]
        for k in key:
            self.application.sendEvent(self.webview, QKeyEvent(QEvent.KeyPress, k, self.application.keyboardModifiers()))
            if wait_for_page_loaded:
                self.loaded = False
            self.sleep(delay)

        if wait_for_page_loaded:
            self.wait_for_page_loaded()
        return self

    def scroll_left(self):
        self.main_frame.scroll(-10, 0)
        self.application.processEvents()

    def scroll_right(self):
        self.main_frame.scroll(10, 0)
        self.application.processEvents()

    def scroll_up(self):
        self.main_frame.scroll(0, -10)
        self.application.processEvents()

    def scroll_down(self):
        self.main_frame.scroll(0, 10)
        self.application.processEvents()

    class AntigateKeyNotDefined(Exception):
        pass

    def solve_captcha(self, img_selector):
        if self.web_app.antigate_key is None:
            raise WebPage.AntigateKeyNotDefined
        tmp = os.path.join(tempfile.gettempdir(), 'captcha_' + random_string() + '.png')
        self.find_elements(img_selector)[0].capture().save(tmp)
        return recognize_captcha(tmp, self.web_app.antigate_key)

    def until_selector(self, key, selector):
        while True:
            if len(self.find_elements(selector)) > 0:
                return self
            self.key(key)

    def set_field_value(self, selector, value, wait_for_page_loaded=False, wait_for_selector=True):
        if wait_for_page_loaded:
            self.wait_for_page_loaded()
        if wait_for_selector:
            self.wait_for_selector(selector)

        def _set_checkbox_value(el, value):
            if value is True:
                el.setAttribute('checked', 'checked')
            else:
                el.removeAttribute('checked')

        def _set_checkboxes_value(els, value):
            for el in els:
                if el.attribute('value') == value:
                    _set_checkbox_value(el, True)
                else:
                    _set_checkbox_value(el, False)

        def _set_radio_value(els, value):
            for el in els:
                if el.attribute('value') == value:
                    el.setAttribute('checked', 'checked')

        def _set_select_value(el, value):
            for _el in el.findAll('option'):
                _el.removeAttribute('selected')
                if _el.attribute('value') == value:
                    _el.setAttribute('selected', '')

        def _set_text_value(el, value):
            el.setAttribute('value', value)

        def _set_textarea_value(el, value):
            el.setPlainText(value)

        element = self.main_frame.findFirstElement(selector)
        if element.isNull():
            raise Exception('can\'t find element for %s"' % selector)
        if element.tagName() == "SELECT":
            _set_select_value(element, value)
        elif element.tagName() == "TEXTAREA":
            _set_textarea_value(element, value)
        elif element.tagName() == "INPUT":
            if element.attribute('type') in ["color", "date", "datetime",
                                             "datetime-local", "email", "hidden", "month", "number",
                                             "password", "range", "search", "tel", "text", "time",
                                             "url", "week"]:
                _set_text_value(element, value)
            elif element.attribute('type') == "checkbox":
                els = self.main_frame.findAllElements(selector)
                if els.count() > 1:
                    _set_checkboxes_value(els, value)
                else:
                    _set_checkbox_value(element, value)
            elif element.attribute('type') == "radio":
                _set_radio_value(self.main_frame.findAllElements(selector),
                    value)
        else:
            raise Exception('unsuported field tag')
        return self

    class CannotFindForm(Exception):
        pass

    def fill_form(self, selector, values):
        if not self.exists(selector):
            raise self.CannotFindForm
        for field, value in values.iteritems():
            self.set_field_value("%s [name=%s]" % (selector, field),
                value)
        return self

    def move_mouse(self, x, y, delay=1000, curve_generator=curvers.Curvers.nonlinear, time_step=10):
        point_b = QPoint(x, y)
        point_a = QCursor.pos()
        dist = ((point_a.x() - point_b.x())**2 + (point_a.y() - point_b.y())**2)**0.5
        steps = int(delay * dist * 0.001 / time_step) + 1
        curve = curve_generator(point_a.x(), point_a.y(), point_b.x(), point_b.y(), steps)

        for x, y in curve:
            QCursor.setPos(QPoint(x, y))
            self.sleep(time_step)

    def click_mouse(self, x=None, y=None, button='left', double_click=False, delay=100):
        point = QPoint(x, y)

        if button == 'left':
            btn = Qt.LeftButton
        elif button == 'right':
            btn = Qt.RightButton

        buttons = Qt.LeftButton | Qt.RightButton
        if double_click:
            event = QMouseEvent(QEvent.MouseButtonDblClick, point, btn, buttons, self.application.keyboardModifiers())
            self.application.sendEvent(self.webview, event)
        else:
            event = QMouseEvent(QEvent.MouseButtonPress, point, btn, buttons, self.application.keyboardModifiers())
            self.application.sendEvent(self.webview, event)
            event = QMouseEvent(QEvent.MouseButtonRelease, point, btn, buttons, self.application.keyboardModifiers())
            self.application.sendEvent(self.webview, event)
        self.sleep(delay)

    def set_file_to_upload(self, filename):
        self._upload_file = filename
        return self